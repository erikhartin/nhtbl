const data = `{
  "maur": {
    "images": [
      "/assets/images/art/MAUR–1.jpg",
      "/assets/images/art/MAUR–2.jpg",
      "/assets/images/art/MAUR–3.jpg"
    ],
    "title": "M.A.U.R.",
    "name": "Erik Pirolt (2019)",
    "description": "<em>(Magisk Altomfattende Underliggjørende Romprogram)</em><br />Verket er en 2,5 x 2 x 1,5 meter stor bronseskulptur av en maur som iakttar naturen utstyrt med astronautdrakt og filmkamera. Nærmere bestemt er kameraet rettet mot en kjempemaurtue, som ligger litt til side for Pilegrimsleden over Kløv. Med plasseringen inne i skogslysningen, med  utsikt over dalen og skjermet av høye trær, har  kunstneren latt seg inspirere av maurtua som  et bilde på naturen som skaperverk, og språket  som en måte å ha bevissthet om skaperverket på.",
    "directions": "<em>M.A.U.R</em>. ligger ved utkikkspunktet Kløv langs  Pilegrimsleden mellom Rindal i sør og Stalsbergsvea  i nord. For å komme til skulpturen må man gå et stykke til fots, ca. 30 min. fra nærmeste parkering ved Rindal, og ca. 20 min. fra parkering ved Stalsbergsvea i enden av Sveavegen.<br />Parkering ved Rindal eller Stalsbergsvea.",
    "maplink": "https://goo.gl/maps/PhzdJ1dXpzHFa9WVA"
  },
  "mammuten": {
    "images": [
      "/assets/images/art/Mamm-1.jpg",
      "/assets/images/art/Mamm-2.jpg",
      "/assets/images/art/Mamm-3.jpg"
    ],
    "title": "Mammuten",
    "name": "Linda Bakke (2016)",
    "description": "<em>Mammuten</em> av Linda Bakke er en tro gjengivelse  av en mammuthodeskalle, gjenskapt i dobbel størrelse. Gudbrandsdalen er unik i nasjonal sammenheng når det gjelder mammutfunn og Fåvang er stedet i Norge med flest registrerte funn. Fra skulpturen <em>Mammuten</em> ser en rett på grustaket der mammuttennene ble funnet. Grusmassene  ble dannet av en stor breelv som også førte med seg de mellom 50.000–100.000 år gamle funnene.",
    "directions": "<em>Mammuten</em> ligger ved Badedammen, like ved avkjøring fra E6 til Fåvang sentrum. <br>Parkering ved Badedammen.",
    "maplink": "https://goo.gl/maps/3svjhxxXfNb9VuoB7"
  },
  "Dragen": {
    "images": [
      "/assets/images/art/Dragen-1.jpg",
      "/assets/images/art/Dragen-2.jpg",
      "/assets/images/art/Dragen-3.jpg"
    ],
    "title": "Dragen",
    "name": "Linda Bakke (2016)",
    "description": "Ringebu stavkirke er den mest besøkte attraksjonen i Ringebu og bakgrunn for skulpturen <em>Dragen</em> av Linda Bakke. Utgangspunkt for kunstverket er en portal med rikt dekorert treskurd i kirkens inngangsparti. Portalen består av 15 drager/slanger som er flettet i hverandre og biter hverandre i halen. Sett forfra er skulpturen <em>Dragen</em> nesten identisk med en av disse dragene, som har blitt «løftet» ut av portalen og fått tredimensjonal form.",
    "directions": "<em>Dragen</em> er plassert i Ringebu sentrum, ved avkjøring til Vekkomsvegen, godt synlig fra gangbru over Våla.<br />Parkering i Ringebu sentrum.",
    "maplink": "https://goo.gl/maps/UZuZa3psuPnzQVLeA"
  },
  "hovudet": {
    "images": [
      "/assets/images/art/Hovud-1.jpg",
      "/assets/images/art/Hovud-2.jpg",
      "/assets/images/art/Hovud-3.jpg",
      "/assets/images/art/Hovud-4.jpg"
    ],
    "title": "Hovudet, Klokkeporten  og Falkesteinen ",
    "name": "Ånond Versto (2020)",
    "description": "Skulpturene er knyttet til sagn fra området  og representerer på gåtefullt vis ulike sider av  pilegrimens reise. <em>Hovudet</em> er knyttet til retterstedet i nærheten, <em>Klokkeporten</em> til sagnet om Søsterklokkene. <em>Falkesteinen</em> er vandrefalken  – falco peregrinus.",
    "directions": "<em>Hovudet, Klokkeporten og Falkesteinen</em> ligger  ved utkikkspunktet Høgkleiva langs Pilegrimsleden mellom Fåvang og Ringebu stavkirke. Fra parkering ved stavkirken kan du følge merking av Pilegrimsleden sørover til Høgkleiva, ca 2 km (30 min. gange).<br />Parkering ved Ringebu stavkirke.",
    "maplink": "https://goo.gl/maps/pveWBzP1pRm5bT6x9"
  },
  "mappa": {
    "images": [
      "/assets/images/art/Mappa-1.jpg",
      "/assets/images/art/Mappa-2.jpg",
      "/assets/images/art/Mappa-3.jpg"
    ],
    "title": "Mappa Mundi",
    "name": "Jumana Manna (2013)",
    "description": "Jumana Mannas <em>Mappa Mundi</em> ved Pilegrimssenteret og Opplands tusenårssted Dale-Gudbrands gard, gir assosiasjoner til monumenter i stein  eller minnesmerker nedlagt på symbolsk grunn. Skulpturen er i dette tilfellet en steinmosaikk som gjengir det såkalte Hereford Mappa Mundi, det mest kjente og eldste bevarte av middelalderens religiøse verdenskart. Den abstrakte gjengivelsen av kartet symboliserer en forbindelse til vår egen tid, der globalisering er allestedsværende og utgjør en fragmentert og porøs grense mot oss selv.",
    "directions": "<em>Mappa Mundi</em> ligger ved Pilegrimssenteret,  Dale-Gudbrands Gard på Hundorp.<br />Parkering ved Dale-Gudbrands gard",
    "maplink": "https://goo.gl/maps/d53PjscqwRi6X7n9A"
  },
  "flokk": {
    "images": [
      "/assets/images/art/Flokk-1.jpg",
      "/assets/images/art/Flokk-2.jpg",
      "/assets/images/art/Flokk-3.jpg"
    ],
    "title": "Flokk",
    "name": "Gitte Dæhlin (2011)",
    "description": "På en åpen plass ved middelaldergården Sygard Grytting i Sør-Fron kommune, står 21 smale skikkelser støpt i bronse og skuer utover Gudbrandsdalslågen. Besøkende som tar avstikkeren fra E6 forventer kanskje først og fremst å møte en flokk beitende sauer, men får først se en flokk langstrakte grasiøse kropper. Figurene har glassøyne og er relativt like, men samtidig helt ulike. De er en gruppe, og hver  især er én.",
    "directions": "<em>Flokk</em> er plassert i et beitelandskap like ned for Gudbrandsdalsvegen ved Sygard Grytting.  Skiltet vei ned til skulpturene.<br />Parkering ved Gudbrandsdalsvegen.",
    "maplink": "https://maps.app.goo.gl/EkpfEeJicBZAqGpE6"
  },
  "oktagon": {
    "images": [
      "/assets/images/art/Okta-1.jpg",
      "/assets/images/art/Okta-2.jpg",
      "/assets/images/art/Okta-3.jpg",
      "/assets/images/art/Okta-4.jpg"
    ],
    "title": "Oktagon",
    "name": "Rintala Eggertsson arkitekter (2017)",
    "description": "På Harpefoss har Rintala Eggertsson arkitekter sammen med 18 arkitektstudenter laget to oktagonale installasjoner, den ene ved Strandtjønn, i overgang mellom land og vann, og den andre ved Hella med utsikt over Gudbrandsdalslågen.  Verket heter <em>Oktagon</em> og begge er konstruert som en åttekantet form, laget i tre. Rintala Eggertsson arkitekter ønsker med sin installasjon på Harpefoss å bremse opp farten litt og gi den besøkende anledning til å oppleve naturen på en roligere måte.",
    "directions": "<em>Oktagon</em> er plassert ved Klokkarstranda ved utløpet av Harpefossjuvet. Merket sti fra Harpefossbrua gjennom skogen ned til Klokkarstranda (20 min gange), eller parkering nede ved Klokkarstranda. <br />Parkering ved Harpefoss hotell og deretter merket sti til installasjon  (20 min.gange), eller Klokkarstranda.",
    "maplink": "https://goo.gl/maps/BTqsAHaAvDaCeXMj9"
  },
  "road": {
    "images": [
      "/assets/images/art/Road-1.jpg",
      "/assets/images/art/Road-2.jpg",
      "/assets/images/art/Road-3.jpg"
    ],
    "title": "The Road",
    "name": "Lars-Andreas Tovey Kristiansen (2017)",
    "description": "<em>The Road</em> av Lars-Andreas Tovey Kristiansen består av fotografier av hus som ble innløste da  ny E6 ble bygget mellom Frya og Sjoa i perioden 2014–2017. Fotografiene er overført til mekanisk variable skilt, tilsvarende de som benyttes langs veien for å vise fleksibel informasjon for blant annet omkjøringer. Fotografiene portretterer  ikke bare de innløste husene, men viser også landskapet før og etter veien ble til og forteller om konsekvenser av slike omfattende veiprosjekter.",
    "directions": "<em>The Road</em> består av to skulpturer med avkjøring Harpefoss/Gudbrandsdalsvegen ved E6, og en skulptur ved avkjøring E6 til Vinstra vegpark.<br />Parkering ved Harpefoss oppvekstsenter og Vinstra vegpark.",
    "maplink": "https://goo.gl/maps/82GkKyNhyzfvBRFa9"
  },
  "gronn": {
    "images": [
      "/assets/images/art/Gronn-1.jpg",
      "/assets/images/art/Gronn-2.jpg",
      "/assets/images/art/Gronn-3.jpg"
    ],
    "title": "Den grønnkledde",
    "name": "Kjell Erik Killi Olsen (2019)",
    "description": "<em>Den grønnkledde</em> av Kjell Erik Killi Olsen er inspirert av Henrik Ibsens <em>Peer Gynt</em>, og ble  laget i forbindelse med at Killi Olsen var festival-kunstner under Peer Gynt stemnet i 2019. Vinstra fikk bystatus i 2013 og valgte da å knytte Peer Gynt, ”Byen i Peer Gynts rike” som identitet  for byutvikling av Vinstra. Den tre meter høye skulpturen i bronse ble kjøpt av Nord-Fron kommune og plassert ved Vinstra stasjon i 2020.",
    "directions": "<em>Den grønnkledde</em> er plassert ved Vinstra togstasjon.<br />Parkering ved Vinstra stasjon.",
    "maplink": "https://goo.gl/maps/dmBnZpqMrHUDmDyWA"
  },
  "mark": {
    "images": [
      "/assets/images/art/Mark-1.jpg",
      "/assets/images/art/Mark-2.jpg"
    ],
    "title": "Markører for  midlertidige samtaler",
    "name": "Dyveke Sanne (2017)",
    "description": "<em>Markører for midlertidige samtaler</em> av Dyveke Sanne er en skulpturell installasjon bestående av to vardelignende konstruksjoner i tre. Ettersom nye steder for samtaler og diskusjoner om utvikling av Vinstra aktualiseres, vil skulpturen kunne flyttes. To varsellys med røde lamper er plassert  på toppene av de to vardene, og er ment å kunne aktiveres og signalisere behov lokalbefolkningen har for å kartlegge et hjemsted i hurtig endring.",
    "directions": "<em>Markører for midlertidige samtaler</em> er plassert på hver side av gamle Sundbrua, gangbrua over Lågen som knytter de to delene av sentrum sammen. Kunstverket kan også oppleves langs Peer Gynt-stien.<br />Parkering i Vinstra sentrum.",
    "maplink": "https://goo.gl/maps/G5PuJKQE7Sdz1e7L7"
  },
  "odel": {
    "images": [
      "/assets/images/art/Odel-1.jpg",
      "/assets/images/art/Odel-2.jpg",
      "/assets/images/art/Odel-3.jpg"
    ],
    "title": "Odelsgut og fantefølge",
    "name": "Felleskapsprosjektet å fortette byen (2019)",
    "description": "Felleskapsprosjektet å fortette byen har med sin skulptur <em>Odelsgut og fantefølge</em> både bidratt til  å sette den lille bygda Kvam på det nasjonale veikartet og skape diskusjoner om hva kunst i offentlige rom er – og kan være. Skulpturen består av en fontene, en stokk formet som en tradisjonell samisk Bealljit, et utskåret dikt av den samiske forfatteren Sigbjørn Skåden, en møkkaspreder kunstnerne fikk av en lokal bonde, en rikule festet  i enden av en lenke og andre detaljer utført av en romsk kobbersmed. Sammen utgjør de elementer  i en fortelling om å reise og ulike møter kunst-prosjektet førte til. ",
    "directions": "<em>Odelsgut og fantefølge</em> er plassert i en liten  park i Kvam sentrum, nedenfor Slettakrysset/Gudbrandsdalsvegen.<br />Parkering i Kvam sentrum.",
    "maplink": "https://goo.gl/maps/4MjhH9dQbUBpbtup7"
  },
  "baret": {
    "images": [
      "/assets/images/art/Baret-1.jpg",
      "/assets/images/art/Baret-2.jpg",
      "/assets/images/art/Baret-3.jpg"
    ],
    "title": "Båret av vinden, ført av strømmen",
    "name": "Jan Christensen og Marius Dahl (2020)",
    "description": "<em>Båret av vinden, ført av strømmen</em> av Marius Dahl og Jan Christensen er en kunstinstallasjon  på Loftgårsdbrua ved Otta. Kunstverket er utformet som to åpne paviljonger med benker  og tak, og fungerer som stoppunkt på brua. Paviljongene i blått og grønt viser til det grønne Ottavassdraget og blåfargen fra Gudbrandsdals-lågen som blander seg sammen der Loftgårdsbrua ligger. Prosjektet ble utviklet i tilknytning til by- og regionsenterplan for Otta, og er produsert og montert av den lokale hjørnesteinsbedriften Lonbakken mekaniske.",
    "directions": "<em>Båret av vinden, ført av strømmen</em> er plassert på gang og sykkelbru Loftsgårsdbrua ved Otta sentrum.<br />Parkering i Otta sentrum.",
    "maplink": "https://maps.app.goo.gl/n9mDV8p4SwpxESPb9"
  },
  "o": {
    "images": [
      "/assets/images/art/O-1.jpg",
      "/assets/images/art/O-2.jpg",
      "/assets/images/art/O-3.jpg"
    ],
    "title": "-=O=-",
    "name": "Pekka Stokke (2016)",
    "description": "Pekka Stokkes kunstprosjekt med tittel <em>-=0=-</em> er plassert på Felleskjøpets kornsilo i Otta, og er et landemerke både dag- og kveldstid. Lyssignalene styres av laboratoriedata som måles fra studielinjen Klimalab på Otta videregående skole,  og som deretter sendes videre til installasjonen. <em>-=0=-</em> er temporært verk, en stor sirkel på siloveggen som både sender sine signaler til byen  Otta og for passerende trafikanter på E6.",
    "directions": "<em>-=0=-</em> er godt synlig på kornsiloen langs E6 enten du ankommer Otta fra sør eller nord. <br />Parkering ved Otta kulturhus.",
    "maplink": "https://goo.gl/maps/HTX7ZNLYQJ2tythe8"
  }
}`

export default data
